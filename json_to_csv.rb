require 'json'

FILE_NAME = 'Barcodes-HS-Dog-Sports'

items = JSON.parse(File.read("#{FILE_NAME}.json")).compact

features_map = []
items.each do |item|
  features_map = (features_map + item['features'].keys).uniq.sort
end

features_map.delete('Artikelnummer')

items.each do |item|
  new_features = features_map.each_with_object({}) { |feature, obj| obj[feature] = item['features'][feature] }
  item['features'] = new_features
end

File.write("#{FILE_NAME}_all_features.json", JSON.pretty_generate(items))

require 'open-uri'
require 'nokogiri'
require 'pry'
require 'json'
require 'active_support'
require 'active_support/core_ext'

require 'net/http'
require 'uri'

FILE_NAME = 'Barcodes-HS-Dog-Sports'

def parse_table(content, selector)
  content.css("table#{selector} tr").each_with_object({}) do |row, obj|
    obj[row.at_css('td:first-child').text.strip] = row.at_css('td:last-child').text
  end
end

def features(content)
  parse_table(content, '.highlights')
end

def search(article_no)
  # uri = URI.parse('https://hundesport.sprenger.de/en/search/?tx_uodata_uodata%5Baction%5D=search&tx_uodata_uodata%5Bcontroller%5D=Search')

  # http = Net::HTTP.new(uri.host, uri.port)
  # request = Net::HTTP::Post.new(uri.request_uri)
  # request.body = "tx_uodata_uodata%5Bsearch%5D%5Bsword%5D=#{article_no}"

  # response = http.request(request)
  `curl 'https://hundesport.sprenger.de/en/search/?tx_uodata_uodata%5Baction%5D=search&tx_uodata_uodata%5Bcontroller%5D=Search' --data 'tx_uodata_uodata%5Bsearch%5D%5Bsword%5D=#{article_no}'`
end

def item_doc(article_no)
  file_path = "items/#{article_no}.html"
  item_html =
    if File.exist?(file_path)
      res = File.read(file_path)
      return if res.empty?
      res
    else
      doc = Nokogiri::HTML(search(article_no))
      # sleep 1
      a = doc&.at_css('.news h3 a')
      unless a
        puts "#{article_no} not found"
        File.write(file_path, '')
        return
      end
      href = a[:href]

      item_html = open(href).read
      File.write(file_path, item_html)
      sleep 1
      item_html
    end
  puts article_no
  Nokogiri::HTML(item_html)
end

def generate_full_json
  items = File.read("#{FILE_NAME}.csv").split("\n").map do |row|
    columns = row.split(',')
    article_no = columns[3]
    item_doc = item_doc(article_no)
    next { columns: columns } unless item_doc

    info = item_doc.at_css('.product_enhancement')

    {
      columns:     columns,
      breadcrumbs: item_doc.at_css('.breadcrumb-item:last-child').text.strip,
      image:       item_doc.at_css('.product.media img')[:src],
      title:       info.at_css('.page-title').text.strip,
      description: item_doc.css('.product.info.detailed').css('.product.attribute.description .value').text.strip,
      bullet_points: info.css('.product_enhancements_block li').map { |li| li.text.strip }.join('. '),
      features:    features(info)
    }
  end

  File.write("#{FILE_NAME}_full.json", JSON.pretty_generate(items))
end

generate_full_json

items = JSON.parse(File.read("#{FILE_NAME}_full.json")).map(&:with_indifferent_access)

features_map = []
items.each do |item|
  next unless item[:features]

  features_map = (features_map + item[:features].keys).uniq.sort
end

# features_map.delete('Artikelnummer')
features_map.delete('SKU')

items.each do |item|
  new_features = features_map.each_with_object({}) { |feature, obj| obj[feature] = item.dig(:features, feature) }
  item[:features] = new_features
end

csv_items = items.map do |item|
  initial_columns = item.delete(:columns)
  features = item.delete(:features).values
  (initial_columns + item.values + features)
end

initial_titles = ['Artikel-Nr.', 'Katalog', 'Training', 'Beschläge', 'EAN-Code:', 'Gewicht', 'Stück/qty.', 'Tarif-Nr.']
parsed_values_titles = ['Breadcrumbs', 'Image', 'Title', 'Description', 'Bullet Points']
header = ([nil] * 3 + initial_titles + parsed_values_titles + features_map)

csv_items.unshift(header)

csv = csv_items.map do |item|
  item.map { |value| "\"#{value}\"" }.join(',')
end.join("\n")

File.write("#{FILE_NAME}_full.csv", csv)
